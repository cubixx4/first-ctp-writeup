# 13374C4D3MY: st3gfuck - 150 - By ~dovahkiin0424
_I have an image with this funny rolled cat. Maybe something is hidden in the image... Can you decode it?_

Here we have this cute cat, c4t.jpg.
First, I tried to check if there are any hints in the exif:

```bash
$ identify -verbose c4t.jpg | grep comment
    comment: Cr4ck 1t!
```

Ok, so how can I crack a jpg?
For this, we will need [stegseek](https://github.com/RickdeJager/stegseek), and the _rockyou.txt_ dictionary. 

```bash
$ stegseek -v c4t.jpg rockyou.txt 
StegSeek version 0.5
based on steghide version 0.5.1
[v] Using stegofile "c4t.jpg".
[v] Running on 6 threads.
[v] Using wordlist file "rockyou.txt".
Progress: 0.00% (0 bytes)           

[i] --> Found passphrase: "coolcat"
reading stego file "c4t.jpg"... done
extracting data... done
checking crc32 checksum... ok
[i] Original filename: "fl4g.txt"
[i] Extracting to "c4t.jpg.out"

$ cat c4t.jpg.out 
+++++ +++++ [->++ +++++ +++<] >++.+ +++++ .<+++ [->-- -<]>- -.+++ +++.<
++++[ ->+++ +<]>+ +++.< +++++ +++[- >---- ----< ]>--- ----- .+.<+ +++++
+[->+ +++++ +<]>+ +++++ +++++ +++.+ +++++ .<+++ ++[-> ----- <]>-. <++++
++[-> ----- -<]>- ----- -.<++ +++++ [->++ +++++ <]>++ +++++ +++++ ++.<+
+++[- >---- <]>-- --.++ +++++ .<+++ [->++ +<]>+ +++++ .<+++ +[->- ---<]
>--.+ +++++ ++.<+ +++++ ++[-> ----- ---<] >---- ----- -.<++ +++++ ++[->
+++++ ++++< ]>+++ +++++ +++.< 
```

Hmm, this code looks familiar! And this is not an encoding, it's [brainfuck](https://en.wikipedia.org/wiki/Brainfuck), a sick programming language. So this is where the challenge title comes from. To be honest, I am not sure if could have been able to draw this conclusion from the title alone. Luckily, many years ago, I came across this language, so I recognized it instantly. I you are curious, there are several languanges like this out there. [Whitespace](https://en.wikipedia.org/wiki/Whitespace_(programming_language)) for example. Try to put this one on paper:))
Anyhow, let's get back to our current challenge. Brainfack can be easily run online here for example: https://www.dcode.fr/brainfuck-language. 

And there it is, our flag!

Result: _flag{34sy_4s_fuck!}_


