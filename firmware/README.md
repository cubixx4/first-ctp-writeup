# 13374C4D3MY: Firmware -- By ~takov751

_There is something fishy, since I updated my router. It's been acting weird lately._

[firmware.bin](https://m4t3sz.gitlab.io/bsc/writeup/ctf/2021/13374C4D3MY/chall/firmware.bin)

Since the description told us this should be a router firmware, there must be a way to extract it's filesystem.
The first article I found was this one about `binwalk`: https://embeddedbits.org/reverse-engineering-router-firmware-with-binwalk/

So let's give it a try, shall we?:D

```bash
$ binwalk firmware.bin
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
53952         0xD2C0          U-Boot version string, "U-Boot 1.1.3 (Mar 19 2018 - 15:36:42)"
66560         0x10400         LZMA compressed data, properties: 0x5D, dictionary size: 8388608 bytes, uncompressed size: 2986732    bytes
1049088       0x100200        Squashfs filesystem, little endian, version 4.0, compression:gzip, size: 3976959 bytes, 522 inodes, blocksize: 131072 bytes, created: 2021-01-07 11:49:59
```
 
Squashfs, that sounds promising. With the article and the `binwalk` help, I was able to extract that filesystem with:

```bash
$ binwalk -e firmware.bin
```

After poking aroung, we can find the router's root filesystem:

```bash
$ ls -l _firmware.bin.extracted/squashfs-root
total 44
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 bin
drwxr-xr-x 5 cubixx cubixx 4096 Mar 19  2018 dev
drwxr-xr-x 5 cubixx cubixx 4096 Jan  7 13:42 etc
drwxr-xr-x 3 cubixx cubixx 4096 Mar 19  2018 lib
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 mnt
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 proc
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 sbin
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 sys
drwxr-xr-x 4 cubixx cubixx 4096 Mar 19  2018 usr
drwxr-xr-x 2 cubixx cubixx 4096 Mar 19  2018 var
drwxr-xr-x 9 cubixx cubixx 4096 Mar 19  2018 web
lrwxrwxrwx 1 cubixx cubixx   11 Mar 19  2018 linuxrc -> bin/busybox
```

After executing `find _firmware.bin.extracted/squashfs-root/ -iname "*flag*"` I got nothing, so we should look elsewhere.
I noticed the directories are pretty old, except the `/etc`. Maybe it makes sense to find the last modified file. As the `/etc` was modified recently, let's try 15 days for a start:

```bash
$ find _firmware.bin.extracted/squashfs-root/ -type f -mtime -15
_firmware.bin.extracted/squashfs-root/etc/init.d/rcS
```
Nothing out of ordinary, but lets make a quick grep on it:

```bash
$ grep flag _firmware.bin.extracted/squashfs-root/etc/init.d/rcS
echo "ZmxhZ3tmMXJtdzRyM19mdW59Cg==" > /etc/flag
```

Wow, the `/etc/flag` is definetely missing, but according to my knowledge, it should not be there anyway. That string looks promising, however.
Let's check it here: https://www.quipqiup.com/ 

Aaaand, it's automagially solved :D

The site also told me, this was a base64 encoded text, which I should have guessed, because of the `==` signs in the end.
So next time I should just use:

```bash
$ echo "ZmxhZ3tmMXJtdzRyM19mdW59Cg==" | base64 -d
flag{f1rmw4r3_fun}
```

Isn't it beautiful? :D






