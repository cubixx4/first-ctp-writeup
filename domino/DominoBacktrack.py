#!/usr/bin/env python3

tokens = []
first = "fla"

def solver(partial, tokens_left):
    if not tokens_left:
        return [partial]

    solutions = []
    for token in tokens_left:
        if token[0:2] == partial[-2:]:
            list_copy = list(tokens_left)
            list_copy.remove(token)

            solutions += solver(partial + token[-1], list_copy)
    return solutions 
    

if __name__ == "__main__":
    with open("domino","r") as myDominoFile:
        for my_line in myDominoFile:
            # Get rid of whitespaces
            my_line = my_line.strip()
            # Get rid of empty tokens
            if my_line:
                tokens.append(my_line.strip())

    # Remove our starting point
    tokens.remove(first)

    print(solver(first, tokens))
