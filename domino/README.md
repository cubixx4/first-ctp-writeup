#13374C4D3MY: domino -- By ~T4r0

_(The flag for this challenge is in Hungarian, to make solving it easier!)_

All we have here is a text file, [domino](https://gitlab.com/cubixx4/first-ctp-writeup/-/blob/master/domino/domino) with some 3 character long lines. Nothing else.
 But actually the title was a really good hint there. If you check the words, you can discover `fla`, `lag` and `ag{` in the file. They match exactly like dominoes. You only need to find the right order.
 
We have our starting point, _fla_, because we know the flag should look like this:
_flag{something_text_whatever}_ 

My first thought was, that after some preparations, I could match those up with simple bash oneliners (I broke it up here a bit, because the line was too long), something like:

```bash
$ wc -l domino
38

# Start our domino with our starting point, and delete it from the list
$ echo -n fla > myDomino
$ sed -i.bak '/^fla$/d' domino

$ for i in {1..38}; do 
    lastPiece=$(grep "^$(tail -c 2 myDomino)" domino | head -n 1); #Look for the next matching domino, but grab only the first match
    sed -i '0,/'"$lastPiece"'/{//d}' domino;                       #Remove that specific match from the list
    echo -n $lastPiece | tail -c 1 >> myDomino;                    #Append myDomino with the last char of the matching line
done
sed: -e expression #1, char 0: no previous regular expression
sed: -e expression #1, char 0: no previous regular expression
sed: -e expression #1, char 0: no previous regular expression
```

But, it's not that simple. `sed` run out of expressions, which means that our `for` cycle finished the sentence, but it didn't used all the words for that. After checking `cat domino`, there are still words in it, so yes, there are multiple possibilities to complete the sentence with these words. 
The flag looks fine in `myDomino`, though. There is only one issue, it is not complete...

```bash
$ cat myDomino 
flag{m3gf3l3l0_s0rr3nd3t}

$ cat domino
t4l
4lt
lt4
4d_
d_4
4_m
gt4
_m3
l4l
m3g
3gt
_4_
4l4
t4d
```

This happened, because there are multiple matches for some words. For example:
When `myDomino` contains the string _flag{m3g_, we are looking for a word, which start with _3g_. Unfortunately we have two matches for that condition: _3gf_ and _3gt_. This means that we have multiple possibilities to go forward. My `for` cycle tried to continue building the domino with first match, always. Let's alter that a bit to see what happens:

```bash
$ cp domino.bak domino; echo -n fla > myDomino; sed -i '/^fla$/d' domino

# Same for cycle, but this time use tail instead of head for grabbing the matching line
$ for i in {1..38}; do 
    lastPiece=$(grep "^$(tail -c 2 myDomino)" domino | tail -n 1); #Get the next matching domino, but grab the last match this time
    sed -i '0,/'"$lastPiece"'/{//d}' domino;                       #Remove that specific match from the list
    echo -n $lastPiece | tail -c 1 >> myDomino;                    #Append myDomino with the last char of the matching line
done
sed: -e expression #1, char 0: no previous regular expression
sed: -e expression #1, char 0: no previous regular expression
sed: -e expression #1, char 0: no previous regular expression
```

I still have the `sed` errors, that't not good. Let's check the results:

```bash
$ cat myDomino 
flag{m3gt4d_4_m3gf3l0_s0rr3nd3t}

$ cat domino
t4l
4lt
lt4
l4l
3l3
4l4
l3l
```

The flag is still incomplete, but this time we have fever lines left in the _domino_ file, so we can guess the flag.

flag{m3gt4l4lt4d_4_m3gf3l3l0_s0rr3nd3t}

So there it is, the flag. But there must be a way to solve this with an algorithm, without manual guessing. There must be a way to list at least all the possibilities. I need more advanced tools here, a backtracking algorithm maybe.

**Gym**, a member of the **0x13374C4D3MY** Discord channel was kind enough, to show me the way. He wrote two python scripts, which I altered slightly. You can find the final forms in this repo. Thank you, **Gym**!

There is a [backtracking](https://gitlab.com/cubixx4/first-ctp-writeup/-/blob/master/domino/DominoBacktrack.py) and an [iterative](https://gitlab.com/cubixx4/first-ctp-writeup/-/blob/master/domino/DominoIterative.py) version. To be honest, I prefer the iterative one, because it makes less operations and it is faster (I made the measurements with the python `timeit` module, but I think that is out of scope for this writeup :D )

```bash
$ python DominoIterative.py 
Found solution: flag{m3gt4l4lt4d_4_m3gf3l3l0_s0rr3nd3t}
```

So that's all, folks!

