#!/usr/bin/env python3

first = "fla"
tokens = []

def solver(partial, tokens_left):
    solutions = [(partial, tokens_left)]
    while solutions:
        next_round = []
        for pair in solutions:
            for token in pair[1]:
                if token[0:2] == pair[0][-2:]:
                    list_copy = list(pair[1])
                    list_copy.remove(token)
                    if not list_copy:
                        print("Found solution: {}".format(pair[0]+ token[-1]))
                    else:
                        #next_round.append((pair[0] + token[-1], list_copy))
                        addNextRound(next_round, (pair[0] + token[-1], list_copy))
        solutions = next_round
    
# Get rid of duplicate possibilities
def addNextRound(rounds, nextRound):
    isAlreadyThere = False
    for my_round in rounds:
        if my_round[0] == nextRound[0]:
            isAlreadyThere = True
            break
    if not isAlreadyThere:
        rounds.append(nextRound)

if __name__ == "__main__":
    with open("domino","r") as myDominoFile:
        for my_line in myDominoFile:
            # Get rid of whitespaces
            my_line = my_line.strip()
            # Get rid of empty tokens
            if my_line:
                tokens.append(my_line.strip())

    tokens.remove(first)
    solver(first, tokens)
