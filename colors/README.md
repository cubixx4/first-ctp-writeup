#13374C4D3MY: colors - 150 - By ~SyrenDuck

_There is a hidden flag in this picture. Good luck._

This challenge gave me a lot of headache. I tried literally every stego tool I found out there, none of them gave me results.
`binwalk` found some strange Zlib compressed data, but it was a false lead, at least I was unable to follow it.

I almost gave up, but after a couple of days, a new idea pop into my head.
The title is colors, right? What if, I should use the hexadecimal representations of the colors present in the image, and convert them to ASCII characters?

I extracted the color codes in the order of appearance with GIMP, and put them in one line, without separator. A hex color code is a combination of three values – the amounts of red, green and blue in a particular shade of color, so we have 3 bytes for every color. Each byte should represent a character, so I made some preprocessing with `sed` to have the hex indicator before each byte, so `echo` can recognize the hex values, and voila, the flag was there:


```bash
$ echo -e $(sed 's#..#\\x&#g' <<< "666c61677b62335f615f7234696e6230777d")
flag{b3_a_r4inb0w}
```

Mindblowing!
