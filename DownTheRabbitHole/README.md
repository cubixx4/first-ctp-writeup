# 13374C4D3MY: Down the rabbit hole - 250 - By ~?

_One of the creators hid a flag (or a clue to get it) on his twitter. See if you can find it._
_By ~Won't tell, it would make the challenge too EZ :)_

And I won't tell you either where, but I found his twitter, and there were some strange tweets indeed:
> 1. 5k2tWE6P7gjtw4iCUSTTeNFaxbTu4cNmjTe3fR1f  
> 2. Sometimes i store my passwords on my personal page but it's hashed so there's no problem with that right?  
> 3. dH8kqBzc7U

To get a clue about those strings, I used https://gchq.github.io/CyberChef. The tool detected `Base58` encoding, and after decoding, I got the following link:
https://pastebin.com/nvVZbqe6

The pastebin link was password protected, however the second string from the twitter profile was accepted as a password. Aaand it gave me a new link:
https://zerobin.hsbp.org/?10a216fdc6a78b25#7Fi7hiD8kibxvqjzAMw4jrkR4s/ZQWd1Li7+JNEhaG0=

Which showed me this string:
```
UEsDBAoACQAAAItuIlLMnGa/JAAAABgAAAAIABwAZmxhZy50eHRVVAkAA4Vs8F+FbPBfdXgLAAEE
AAAAAAQAAAAA7xi8hvm8gaMMVRplw9NAsi//XToJblGLnhv8KIES8/Dbf/XmUEsHCMycZr8kAAAA
GAAAAFBLAQIeAwoACQAAAItuIlLMnGa/JAAAABgAAAAIABgAAAAAAAEAAACkgQAAAABmbGFnLnR4
dFVUBQADhWzwX3V4CwABBAAAAAAEAAAAAFBLBQYAAAAAAQABAE4AAAB2AAAAAAA=
```

That looked like some `base64` encoded stuff:

```bash
$ echo 'UEsDBAoACQAAAItuIlLMnGa/JAAAABgAAAAIABwAZmxhZy50eHRVVAkAA4Vs8F+FbPBfdXgLAAEE
AAAAAAQAAAAA7xi8hvm8gaMMVRplw9NAsi//XToJblGLnhv8KIES8/Dbf/XmUEsHCMycZr8kAAAA
GAAAAFBLAQIeAwoACQAAAItuIlLMnGa/JAAAABgAAAAIABgAAAAAAAEAAACkgQAAAABmbGFnLnR4
dFVUBQADhWzwX3V4CwABBAAAAAAEAAAAAFBLBQYAAAAAAQABAE4AAAB2AAAAAAA=' | base64 -d > mystery

$ file mystery 
mystery: Zip archive data, at least v1.0 to extract

$ mv mystery mystery.zip
$ unzip mystery.zip 
Archive:  mystery.zip
[mystery.zip] flag.txt password: 
```

So this is a password protected `zip` file. Unforunately the same password as before did not work, and `fcrackzip` was unsuccessful. But the twitter message told us something about a password on a personal profile. I found an interesting looking hash 
```
2ab96390c7dbe3439de74d0c9b0b1767
```
on his Discord profile. 

After checking with several `md5` decrypters, it turned out to be _hunter2_, but this wasn't the password:/.

On his twitter, there was a link to his gitlab blog, and there was a blogpost, titled _Challenge_, posted on the same date as the tweets.
The post lead me to the Rick Astley song, Never Gonna Give You Up on YouTube.

Seriously, dude?! It never get's old:|. Or is this a hint?

Let me tell you, it was not. I tried to crack the zip with custom generated wordlists, based on the artist and the song, without luck. Luckily, I got some help from the challenge writer:
> Use the source, Luke!

And there it was, in the blogpost's source code:
```bash
$ curl --silent THELINKTOTHEBLOGPOST | grep -i password
<!-- I told you I keep my passwords on my personal page, but I didn't say they are in plain sight :) : b0f2fb2c277a8263197d8b8bd12bd42258a6fd59e5a5a5cb72b7aa5bbea05bf4 -->
```
I used [md5decrypt.net](https://md5decrypt.net/en/HashFinder/) to analyse (it was Sha256) and decrypt the hash:
> p@5sword

You got to be kidding me:|.

Anyhow, the password worked, and finally I got the flag:
> flag{051N7_W17H_CRYP70?}


